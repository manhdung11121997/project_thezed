import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
export default function CartFood(props) {
    
    const getAPIOrderItem = () => {
        axios.get("http://localhost:8080/orderItems/listOrderItems").then((response) => {
          props.setDetailFood(response.data);
        });
      }
    /*** Function handle Acs and Des */
    const handleIncreaseValue = (orderItemID, quantity, price) => {
        // Increase the quantity by 1
        const plusQuantity = quantity + 1;
        const updateTotal = parseFloat(plusQuantity * price)
        axios.put("http://localhost:8080/orderItems/updateOrderItems", null,
            {
                params: {
                    quantity: plusQuantity,
                    orderItemsID: orderItemID,
                    total: updateTotal,
                },
            }
        )
            .then(() => {
                // Cập nhật lại danh sách thức ăn trong giỏ hàng
                getAPIOrderItem()
            });
    };

    const handleDecreaseValue = (orderItemID, quantity, price) => {
        // Decrease the quantity by 1
        if(quantity === 1){
            return;
        }
        const minusQuantity = quantity - 1
        const updateTotal = parseFloat(minusQuantity * price)
        axios.put("http://localhost:8080/orderItems/updateOrderItems", null,
            {
                params: {
                    quantity: minusQuantity,
                    orderItemsID: orderItemID,
                    total: updateTotal,
                },
            }
        )
            .then(() => {
                // Cập nhật lại danh sách thức ăn trong giỏ hàng
                getAPIOrderItem()
            });
    };

    /*** Function view Cart  */
    function viewCartFoodDetail(food) {
        return food.map((course) => {
            return (
                <div className="row mb-4 d-flex justify-content-between align-items-center">
                    <div className="col-md-2 col-lg-2 col-xl-2">
                        <img src={`http://localhost:8080/food/${course.imageFoodDB}`} className="img-fluid rounded-3" alt="Cotton T-shirt" />
                    </div>
                    <div className="col-md-3 col-lg-3 col-xl-3">
                        <h6 className="text-white mb-0 " id="view-cartFood-foodName">{course.foodName}</h6>
                    </div>
                    <div className="col-md-3 col-lg-3 col-xl-3" style={{ display: "none" }}>
                        <h6 className="mb-0 " id="view-cartFood-foodID">{course.orderItemID}</h6>
                    </div>
                    <div className="col-md-3 col-lg-3 col-xl-2 d-flex">
                        <button className="btn btn-link px-2" id="minus-quantity" onClick={() => handleDecreaseValue(course.orderItemID, course.quantity, course.price)}>
                            <i className="fas fa-minus"></i>
                        </button>

                        <input  id="cartFood-quantity" min="0" name="quantity" value={course.quantity} type="number" className="form-control form-control-sm d-flex text-align-center" />

                        <button className="btn btn-link px-2" id="plus-quantity" onClick={() => handleIncreaseValue(course.orderItemID, course.quantity, course.price)}>
                            <i className="fas fa-plus"></i>
                        </button>
                    </div>
                    <div className="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
                        <h6 className=" text-white mb-0" id="view-cartFood-price">{course.price.toLocaleString("vi-VN") + "₫"}</h6>
                    </div>
                    <div className="col-md-3 col-lg-2 col-xl-2 offset-lg-1" style={{ display: 'none' }}>
                        <h6 className="mb-0">{course.total.toLocaleString("vi-VN") + "₫"}</h6>
                    </div>
                    <div className="col-md-1 col-lg-1 col-xl-1 text-end btnDelete">
                        <span className="product-item-icon" onClick={() => props.deleteCartItem(course.orderItemID)} ><i className="fas fa-times "></i></span>
                    </div>
                    <hr className="my-4" />
                </div>
            )
        })
    }
    
    return (
        <div>
            <section className="h-100 h-custom">
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col-12">
                            <div className="card card-registration card-registration-2" >
                                <div className="card-body p-0">
                                    <div className="row g-0">
                                        <div className="col-lg-8">
                                            <div className="p-5">
                                                <div className="d-flex justify-content-between align-items-center mb-5">
                                                    <h1 className="fw-bold mb-0 text-white">Giỏ hàng</h1>
                                                    <h6 className="mb-0 product-item-icon">{props.countFoodName} sản phẩm</h6>
                                                </div>
                                                <hr className="my-4" />
                                                <div id="viewCartFoodDetail">
                                                    {/* Render view Cart Food */}
                                                    {viewCartFoodDetail(props.apiDetailFood)}
                                                </div>
                                                <div className="pt-5">
                                                    <h6 className="mb-0"><Link to="/MenuFood" className="" style={{ color: 'white' }}><i className="fas fa-long-arrow-alt-left me-2"></i>Quay lại</Link></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-4 bg-grey">
                                            <div className="p-5">
                                                <h3 className="fw-bold mb-5 mt-2 pt-1">Tóm tắt đơn hàng</h3>
                                                <hr className="my-4" />
                                                <div className="d-flex justify-content-between mb-4">
                                                    <h5 className="text-uppercase product-item">{props.countFoodName} sản phẩm</h5>
                                                    <h5 className="cartFood-total">{props.totalPriceFormat}</h5>
                                                </div>
                                                <h5 className="text-uppercase mb-3">Vận chuyển</h5>
                                                <div className="mb-4 pb-2">
                                                    <select className="form-select" id="cartFood-shipping" onChange={props.handleShippingChange}>
                                                        <option value="15000">Giao hàng trong bán kính 5km - 15.000 đ</option>
                                                        <option value="20000">Giao hàng trong bán kính 5-10km - 20.000 đ</option>
                                                        <option value="30000">Giao hàng trong bán kính trên 10km - 30.000 đ</option>
                                                    </select>
                                                </div>
                                                <h5 className="text-uppercase mb-3">Nhập mã giảm giá</h5>
                                                <div className="mb-5">
                                                    <div className="form-outline">
                                                        <input type="text" id="form3Examplea2" className="form-control form-control-lg" />
                                                        <label className="form-label" htmlFor="form3Examplea2">Nhập mã giảm giá của bạn</label>
                                                    </div>
                                                </div>
                                                <hr className="my-4" />
                                                <div className="d-flex justify-content-between mb-5">
                                                    <h5 className="text-uppercase" >Tổng Tiền</h5>
                                                    <h5 className="cardFood-finalTotal">{props.totalPriceFinal}</h5>
                                                </div>
                     
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}
