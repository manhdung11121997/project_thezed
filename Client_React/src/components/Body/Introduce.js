import React from 'react'
import { Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Introduce() {
    /*** Function load to the top of the page */
    const handleClick = () => {
        window.scrollTo(0, 0);
    };
    return (
        <div>
            <main>
                <Carousel fade interval={1500}>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="/images/Banner_QuangCao/Banner1.jpg "
                            alt="First slide"
                        />
                        <Carousel.Caption>

                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="/images/Banner_QuangCao/Banner2.jpg "
                            alt="Second slide"
                        />
                        <Carousel.Caption>

                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="/images/Banner_QuangCao/Banner3.jpg "
                            alt="Third slide"
                        />
                        <Carousel.Caption>

                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </main>
            {/* Menu List */}
            <div className="container car-menu-text">
                <div className="menuText">
                    <h1>MENU</h1>
                    <hr />
                </div>
                <div className="text">
                    <p>Menu TheZed đa dạng và phong phú, gồm hơn 100 món ăn hấp dẫn từ khắp nơi trên thế giới.
                        Chúng tôi chuyên tâm đem đến cho khách hàng những trải nghiệm ẩm thực đa văn hóa,
                        từ các món nướng thơm ngon, đặc sắc, đến các món hấp, xào tinh tế.
                        Hãy đến và thưởng thức những món ăn tuyệt hảo tại TheZed,
                        nơi mang đến cho bạn những khoảnh khắc ẩm thực độc đáo và thú vị.</p>
                </div>
            </div>
            <div className="container mx-auto mt-4">
                <div className="row menu-food-image">
                    <div className="col-md-4">
                        <div className="card card_hover" style={{ width: '18rem' }}>
                            <Link to="/MenuFood?selectedMenu=ĐẶC BIỆT" onClick={handleClick}> <img src="/images/menus/menu2.jpg " className="card-img-top" alt="..." /></Link>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card card_hover" style={{ width: '18rem' }}>
                            <Link to="/MenuFood?selectedMenu=HẢI SẢN" onClick={handleClick}> <img src="/images/menus/menu1.jpg " className="card-img-top" alt="..." /></Link>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card card_hover" style={{ width: '18rem' }}>
                            <Link to="/MenuFood?selectedMenu=LAI RAI" onClick={handleClick}> <img src="/images/menus/menu3.jpg " className="card-img-top" alt="..." /></Link>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card card_hover" style={{ width: '18rem' }}>
                            <Link to="/MenuFood?selectedMenu=LẨU" onClick={handleClick}> <img src="/images/menus/menu4.jpg " className="card-img-top" alt="..." /></Link>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card card_hover" style={{ width: '18rem' }}>
                            <Link to="/MenuFood?selectedMenu=SALAD" onClick={handleClick}> <img src="/images/menus/menu5.jpg " className="card-img-top" alt="..." /></Link>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card card_hover" style={{ width: '18rem' }}>
                            <Link to="/MenuFood?selectedMenu=MÓN THỊT" onClick={handleClick}> <img src="/images/menus/menu6.jpg " className="card-img-top" alt="..." /></Link>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card card_hover" style={{ width: '18rem' }}>
                            <Link to="/MenuFood?selectedMenu=CƠM MÌ CHÁO" onClick={handleClick}> <img src="/images/menus/menu7.jpg " className="card-img-top" alt="..." /></Link>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card card_hover" style={{ width: '18rem' }}>
                            <Link to="/MenuFood?selectedMenu=NƯỚNG" onClick={handleClick}> <img src="/images/menus/menu8.jpg " className="card-img-top" alt="..." /></Link>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    )
}
