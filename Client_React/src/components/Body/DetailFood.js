import React, { useState } from 'react'
import { Outlet, useLocation } from 'react-router-dom';
import Carousel from 'react-multi-carousel';


export default function DetailFood(props) {


  /*** get location (URL) form hook useLocation */
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);

  //  get Detail Food form URL
  const image = queryParams.get('image');
  const foodName = queryParams.get('foodName');
  const price = queryParams.get('price');
  const description = queryParams.get('description');

  /*** Responsive has Carousel */
  const responsive = {
    superLargeDesktop: {

      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 4
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

  return (
    <div>
      <div className="container">
        <div className="row food-detail" id="detai-Food">
          <div className="col-md-6 food-detail-left">
            <div className="detail-left-img">
              <div className="card zoom">
                <div className="image-zoom detail-food-image">
                  <img src={`http://localhost:8080/food/${image}`} className="" alt="..." />
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 food-detail-right">
            <div className="detail-right-name">
              <h4 className="food-nameFood">{foodName}</h4>
            </div>
            <div className="detail-right-text">
              <p className="food-description">{description}</p>
            </div>
            <div className="detail-right-pice">
              <p className="price">
                <del aria-hidden="true">
                  <bdi>90.000₫</bdi>
                </del>
                <ins>
                  <bdi className="food-price">{price}₫</bdi>
                </ins>
              </p>
            </div>
            <hr />

            <div className="detail-right-button">
              <div className="col-md-3">
                <div className="input-group">
                  <form>
                    <div className="value-button btn btn-outline-secondary btn-number" id="decrease"
                      onClick={props.handleDecreaseValue}>-</div>
                    <input type="number" id="number" className="food-quantily" value={props.quantityValue} />
                    <div className="value-button btn btn-outline-secondary btn-number" id="increase"
                      onClick={props.handleIncreaseValue}>+</div>
                  </form>
                </div>
              </div>
              <div className="col-md-5">
                <button type="button" className="btn btn-warning" id="addCartFood" onClick={() => props.handleAddToCart(foodName)} > Thêm giỏ hàng</button>
              </div>
            </div>
            <hr />
            <div className="detail-right-note">
              <ul>
                <li>Miễn phí vận chuyển toàn cầu cho tất cả các đơn hàng</li>
                <li>Đổi trả dễ dàng trong 30 ngày nếu bạn đổi ý</li>
                <li>Đặt hàng trước buổi trưa để giao hàng trong ngày</li>
              </ul>
            </div>
            <div className="detail-right-checkout">
              <div className="card-icon-text">
                <p>Thanh toán an toàn được đảm bảo</p>
              </div>
              <div className="card-icon-box">
                <div className="card-icon" style={{ backgroundImage: "url('/images/iconCard/visa.png')", filter: 'brightness(1.5)' }}></div>
                <div className="card-icon" style={{ backgroundImage: "url('/images/iconCard/MasterCard.png')", filter: 'brightness(0.8)' }}></div>
                <div className="card-icon" style={{ backgroundImage: "url('/images/iconCard/momo.jpg')", filter: 'brightness(1.2)' }}></div>
                <div className="card-icon" style={{ backgroundImage: "url('/images/iconCard/ZaloPay.png')", filter: 'brightness(1.0)' }}></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Carousel */}
      <div className="food-detail-related">
        <div className="food-detail-related-name">
          <h4>SẢN PHẨM TƯƠNG TỰ</h4>
        </div>
      </div>
      <div className="container mx-auto mt-4">
        <div className="row menu-food-image">
          <Carousel className="testimonial-carousel wow fadeInUp" id="detail-food" responsive={responsive} swipeable={true}
            draggable={true}
            data-wow-delay="0.1s"
            interval={5000}
            itemClass="carousel-item-padding-10-px">
            {props.renderAPIFood(props.apiFood)}
          </Carousel>

         
        </div>
      </div>
      <Outlet />
    </div>


  )
}
