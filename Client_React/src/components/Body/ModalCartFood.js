import React, { useEffect, useState } from 'react';
import { Modal, Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ModalCartFood(props) {

  // Khoi tao fuction de render API
  function renderAPIModalContent(detailFood) {
    return detailFood.map((course) => {
      return (
        <tr key={course.orderItemID}>
          <td className="product-Modal">{course.orderItemID}</td>
          <td className="w-25">
            <img src={`http://localhost:8080/food/${course.imageFoodDB}`} className="img-fluid img-thumbnail" alt="Product Image" />
          </td>
          <td className="product-Modal">{course.foodName}</td>
          <td className="price-Modal">{course.price.toLocaleString("vi-VN") + "₫"
          }</td>
          <td className="quantity-Modal">
            <input type="number" className="form-control quantity-input" defaultValue={course.quantity} />
          </td>
          <td className="totalFood">{course.total.toLocaleString("vi-VN") + "₫"
          }</td>
          <td>
            <a href="#" className="btn-sm btnDelete" onClick={() => props.deleteCartItem(course.orderItemID)} >
              <i className="fa fa-times"></i>
            </a>

          </td>
        </tr>
      )

    })

  }
  const handleCloseModal = () => {
    props.closeModal(false);
  };
  return (
    <>
      <Modal show={props.showModal} onHide={handleCloseModal} size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Giỏ hàng của bạn</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table responsive striped bordered hover className='table table-image'>
            <thead>
              <tr>
                <th scope="col"></th>
                <th scope="col">Sản phẩm</th>
                <th scope="col">Giá</th>
                <th scope="col">Số lượng</th>
                <th scope="col">Tổng</th>
                <th scope="col">Thao tác</th>
              </tr>
            </thead>
            <tbody id="cart-food-modal">
              {renderAPIModalContent(props.apiDetailFood)}

            </tbody>
          </Table>
          <div className="d-flex justify-content-end">
            <h5 className="text-total-modal">Tổng: <span id="totalPrice" className="price text-danger">{props.totalPriceFormat}</span></h5>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <div className="box-btn">
            <Link to="/cartFood" >
              <Button variant="danger" id="viewCartFood" onClick={props.closeModal}  >
                Xem giỏ hàng
              </Button>
            </Link>
      
          </div>
        </Modal.Footer>
      </Modal>
    </>
  )
}

