import React from 'react';
import { Link, Outlet } from 'react-router-dom';

export default function HeaderFood(props) {
    return (
        <div>
            <header>
                <div className="container">
                    <div className="wrap">
                        <div className="row">
                            <div className="col-md-6 d-flex align-items-center">
                                <p className="mb-0 phone pl-md-2">
                                    <a href="#" className="mr-2"><span className="fa fa-phone me-1"></span> 035 9188 975  </a>
                                    <a href="#"><span className="fa fa-paper-plane me-1"></span> thezed.grillandbeer@gmail.com</a>
                                </p>
                            </div>
                            <div className="col-md-6 d-flex justify-content-md-end">
                                <div className="social-media mr-4">
                                    <p className="mb-0 d-flex">
                                        <a href="#" className="d-flex align-items-center justify-content-center"><span
                                            className="fa fa-facebook"><i className="sr-only">Facebook</i></span></a>
                                        <a href="#" className="d-flex align-items-center justify-content-center"><span
                                            className="fa fa-twitter"><i className="sr-only">Twitter</i></span></a>
                                        <a href="#" className="d-flex align-items-center justify-content-center"><span
                                            className="fa fa-instagram"><i className="sr-only">Instagram</i></span></a>
                                        <a href="#" className="d-flex align-items-center justify-content-center"><span
                                            className="fa fa-dribbble"><i className="sr-only">Dribbble</i></span></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav>
                        <div className="row">
                            <ul className="icon-list">
                                <div className="box-logo">
                                    <img src="/images/logo/logo2.jpg" alt="" className="header-image" />
                                </div>
                                <div className="box-search">
                                    <button type="button" className="btn btn-danger dropdown-toggle" data-bs-toggle="dropdown">
                                        <span id="search_concept">Filter by</span> <span className="caret"></span>
                                    </button>
                                    <ul className="dropdown-menu dropdown-menu-right">
                                        <li><a className="dropdown-item" href="#">Contains</a></li>
                                        <li><a className="dropdown-item" href="#">It's equal</a></li>
                                        <li><a className="dropdown-item" href="#">Greater than</a></li>
                                        <li><a className="dropdown-item" href="#">Less than</a></li>
                                    </ul>
                                    <div className="input-group">
                                        <div id="search-autocomplete" className="form-outline">
                                            <input type="search" id="form-search" className="form-control" placeholder="Search term..." />
                                        </div>
                                        <button type="button" className="btn btn-danger">
                                            <i className="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div className="box-icon">
                                    <li className="rounded-icon">
                                        <a href="#"><i className="fa fa-search"></i></a>
                                    </li>
                                    <li className="rounded-icon">
                                        <a href=""><i className="fa fa-user"></i></a>
                                    </li>
                                    <li className="rounded-icon">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#cartFoodModal" onClick={props.handleCartIconClick}>
                                            <i class="fa fa-shopping-cart"></i>
                                            <span id="cartItemCount" className="cart-item-count ">{props.countFoodName}</span>
                                        </a>
                                    </li>

                                </div>
                            </ul>
                        </div>
                        <div className="row-menu">
                            <ul className="row-menu-nav">
                                <li className="menu-item"><Link to="/">GIỚI THIỆU</Link></li>

                                <li className="dropdown menu-item">
                                    <a href="#">THỰC ĐƠN</a>
                                    <ul className="dropdown-menu">
                                        <li><Link to="/MenuFood">Danh mục</Link>
                                        </li>
                                        <li><Link to="/DetailFood?foodID=1&image=5fbef15b6b184d2d856e12923bba05e4.jpg&foodName=Chả%20Ốc%20Bươu%20Chiên%20Giòn&price=90.000&description=Món%20Chả%20ốc%20bươu%20chiên%20giòn%20là%20sự%20kết%20hợp%20độc%20đáo%20giữa%20ốc%20bươu%20tươi%20và%20thịt%20heo%20nhuyễn,%20tạo%20nên%20chả%20chiên%20giòn%20ngoài,%20mềm%20ngọt%20bên%20trong.%20Chả%20ốc%20bươu%20được%20chiên%20vàng%20giòn,%20mang%20đến%20hương%20vị%20thơm%20ngon,%20kích%20thích%20vị%20giác.%20Thực%20phẩm%20chất%20lượng,%20nguyên%20liệu%20tươi%20ngon,%20và%20các%20gia%20vị%20tự%20nhiên%20được%20sử%20dụng.%20Hãy%20khám%20phá%20hương%20vị%20độc%20đáo%20của%20món%20ăn%20này%20và%20thưởng%20thức%20cùng%20gia%20đình%20và%20bạn%20bè">Chi Tiết</Link>
                                        </li>
                                    </ul>
                                </li>
                                <li className="dropdown menu-item"><a href="#">QUẢN LÝ</a>
                                    <ul className="dropdown-menu">

                                        <li><Link to="/FoodManager">Menu</Link></li>

                                    </ul>
                                </li>
                            </ul>


                        </div>
                    </nav>
                </div>
            </header>
            <Outlet />
        </div>
    )
}
