import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <div>
               <footer>
  <div className="container-fluid footer mt-5 pt-5 wow fadeIn" data-wow-delay="0.1s">
    <div className="container py-5">
      <div className="row g-5">
        <div className="col-lg-3 col-md-6">
          <h1 className="fw-bold text-primary mb-4">The<span className="text-secondary"> Zed</span></h1>
          <p>Một trang web bán đồ ăn trực tuyến đáng tin cậy. Chúng tôi mang đến cho bạn những món ăn ngon và hấp dẫn nhất. Hãy khám phá và thưởng thức!</p>
          <div className="d-flex pt-2">
            <a className="btn btn-square btn-outline-light rounded-circle me-1" href=""><i className="fab fa-twitter"></i></a>
            <a className="btn btn-square btn-outline-light rounded-circle me-1" href=""><i className="fab fa-facebook-f"></i></a>
            <a className="btn btn-square btn-outline-light rounded-circle me-1" href=""><i className="fab fa-youtube"></i></a>
            <a className="btn btn-square btn-outline-light rounded-circle me-0" href=""><i className="fab fa-linkedin-in"></i></a>
          </div>
        </div>
        <div className="col-lg-3 col-md-6">
          <h4 className="text-light mb-4">Địa chỉ</h4>
          <p><i className="fas fa-map-marker-alt me-2"></i>03 Kim Đồng, Eakar, Đắk Lắk</p>
          <p><i className="fas fa-phone-square me-2"></i>+035 9188 975</p>
          <p><i className="fa fa-envelope me-2"></i>thezed.grillandbeer@gmail.com</p>
        </div>
        <div className="col-lg-3 col-md-6">
          <h4 className="text-light mb-4">Liên kết nhanh</h4>
          <a className="btn btn-link" href="">Về chúng tôi</a>
          <a className="btn btn-link" href="">Liên hệ</a>
          <a className="btn btn-link" href="">Dịch vụ của chúng tôi</a>
          <a className="btn btn-link" href="">Điều khoản & Điều kiện</a>
          <a className="btn btn-link" href="">Hỗ trợ</a>
        </div>
        <div className="col-lg-3 col-md-6">
          <h4 className="text-light mb-4">Bản tin</h4>
          <p>Nhận tin tức mới nhất và các ưu đãi đặc biệt.</p>
          <div className="position-relative mx-auto" style={{ maxWidth: "400px" }}>
            <input className="form-control bg-transparent w-100 py-3 ps-4 pe-5" type="text" placeholder="Email của bạn" />
            <button type="button" className="btn btn-danger py-2 position-absolute top-0 end-0 mt-2 me-2">Đăng ký</button>
          </div>
        </div>
      </div>
    </div>
    <div className="container-fluid copyright">
      <div className="container">
        <div className="row">
          <div className="col-md-6 text-center text-md-start mb-3 mb-md-0">
            &copy; <a href="#">Tên Trang Web Của Bạn</a>, Bảo lưu mọi quyền.
          </div>
          <div className="col-md-6 text-center text-md-end">
            Thiết kế bởi <a href="https://htmlcodex.com">HTML DungLM6</a>
            <br />Phân phối bởi: <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>


            </div>
        )
    }
}
