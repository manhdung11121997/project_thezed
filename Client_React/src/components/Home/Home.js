import React, { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import axios from 'axios';
import HeaderFood from '../Header/HeaderFood'
import MenuFood from '../Body/MenuFood'
import ModalCartFood from '../Body/ModalCartFood'
import Footer from '../Footer/Footer'
import DetailFood from '../Body/DetailFood';
import CartFood from '../Body/CartFood';
import FoodManager from '../Management/FoodManager';
import Introduce from '../Body/Introduce';



export default function Home() {
  /** Luu tru va thay doi useState*/
  const [apiFood, setApiFood] = useState([]);
  const [apiDetailFood, setDetailFood] = useState([]);

  // Caculator in Modal Cart Food
  const [totalPrice, setTotalPrice] = useState(0);

  // Define show Modal
  const [showModal, setShowModal] = useState(false);

  // Define show Edit Modal
  const [showEditModal, setShowEditModal] = useState(null);

  // Define Count product in Cart Modal
  const [countFoodName, setCountFoodName] = useState(0);

  // DetailFood on URL
  const [selectedFoodDetail, setSelectedFoodDetail] = useState();

  // ShippingPrice
  const [shippingPrice, setShippingPrice] = useState(15000);

  // Define flag assignment
  const [isAddCartFood, SetIsAddCartFood] = useState(false);

  // Selected Quantity
  const [selectedQuantity, setSelectedQuantity] = useState(1);

  /** List product call API*/
  //Render Call API ListFood
  useEffect(() => {
    axios.get("http://localhost:8080/food/listFood").then((response) => {
      const APIFood = response.data;
      setApiFood(APIFood);
    });
  }, []);

  //Render Call API ListOrderItems
  useEffect(() => {
    axios.get("http://localhost:8080/orderItems/listOrderItems").then((response) => {
      const APIFood = response.data;
      const countFood = response.data.length;
      setDetailFood(APIFood);
      setCountFoodName(countFood);
    })
  }, []);

  //Render Calculate total and Item count the cart changes
  useEffect(() => {
    calculateTotal();
    currentCartItemCount();
  }, [apiDetailFood]);

  /*** Funciton get API axios */
  const getAPIOrderItem = () => {
    axios.get("http://localhost:8080/orderItems/listOrderItems").then((response) => {
      setDetailFood(response.data);
    });
  }

  /*** Function getAPIFood */
  const getAPIFood = () => {
    axios.get("http://localhost:8080/food/listFood").then((response) => {
      setApiFood(response.data);
      setShowModal(false)
      setShowEditModal(false);
    })
  }


  /** Add product to Cart Modal Food */
  const handleAddToCart = (foodName) => {
    const selectedProduct = apiFood.find((product) => product.foodName === foodName);
    const selectedFoodName = selectedProduct.foodName;
    const selectedPrice = selectedProduct.price;
    const selectQuantity = selectedQuantity;
    const totalFood = parseFloat(selectedPrice) * selectQuantity;

    // Check if there is a matching element in apiDetailFood
    const matchingElement = apiDetailFood.find(
      (element) => element.foodName === selectedFoodName
    );
    if (matchingElement) {
      // If there is a match, update the order item in the database
      const elementQuantity = parseInt(matchingElement.quantity);
      const elementOrderItemID = matchingElement.orderItemID;
      const elementPrice = matchingElement.price;
      const selectQuantityNew = elementQuantity + selectedQuantity;
      const totalNew = selectQuantityNew * elementPrice;
      axios
        .put(
          "http://localhost:8080/orderItems/updateOrderItems",
          null,
          {
            params: {
              quantity: selectQuantityNew,
              total: totalNew,
              orderItemsID: elementOrderItemID,
            },
          }
        )
        .then(() => {
          // update successful, call API to get List order items
          getAPIOrderItem();
          setSelectedQuantity(1);
        });
    } else {
      // If there is no match, add a new order item to the database
      const orderItems = {
        foodID: selectedProduct.foodID,
        quantity: selectQuantity,
        total: totalFood,
      };
      axios
        .post("http://localhost:8080/orderItems/addOrderItems", orderItems)
        .then(() => {
          // Allow adding to Cart
          SetIsAddCartFood(true);
          // add successful, call API to get List order items
          getAPIOrderItem();
          setSelectedQuantity(1)
        })
        .catch((error) => {
          console.error("Error adding order items:", error);
        });

    }
  };

  /*** Fuction show Modal and Close */
  const handleCartIconClick = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  /*** Funciton to handleDetail  */
  const handleFoodDetailClick = (course) => {
    setSelectedFoodDetail(course);
  };

  /*** Fuction quantity dec and asc  */
  const increaseValue = () => {

    setSelectedQuantity((prevQuantity) => prevQuantity + 1);
  }
  const decreaseValue = () => {
    setSelectedQuantity((prevQuantity) => (prevQuantity > 1 ? prevQuantity - 1 : 1));
  }
  /*** Fuction Cart Modal Food */
  // Count product in Cart
  const currentCartItemCount = () => {
    const countFood = apiDetailFood.length;
    setCountFoodName(countFood);
  }

  // Calculator Total price product in Cart
  const calculateTotal = () => {
    let totalPrice = 0;
    apiDetailFood.forEach((course) => {
      const totalFood = parseFloat(course.total);
      totalPrice += totalFood;
    });
    setTotalPrice(totalPrice);
  }

  //Format totalPrice in VNĐ
  function totalPriceFormat() {
    return totalPrice.toLocaleString("vi-VN") + "₫";
  }
  // fuction Format Money
  const totalPriceFinal = () => {
    return (totalPrice + shippingPrice).toLocaleString("vi-VN") + "₫"
  }

  /*** Function calculator  */
  const handleShippingChange = (event) => {
    const newShippingPrice = parseInt(event.target.value); // Get new value from event
    // update value new 
    setShippingPrice(newShippingPrice);

  };

  /*** Function Render API Food  */
  function renderAPIFood(menu) {
    return menu.map((course) => {
      // Xu ly mo ta toi thieu tren dong
      let shortenedDescription = course.description;
      const maxWords = 25; // So tu toi thieu
      if (course.description.split(" ").length > maxWords) {
        const words = course.description.split(" ");
        shortenedDescription = words.slice(0, maxWords).join(" ");
        shortenedDescription += "...";
      }

      // format numbers into money
      const formattedPrice = course.price.toLocaleString("vi-VN");
      return (
        <div className="col-md-4" key={course.foodID}>
          <div className="card custom-card">
            <img src={`http://localhost:8080/food/${course.imageFoodDB}`} className="card-img-top imageFood" alt="..." />
            <div className="card-body">
              <div style={{ display: "none" }}><h1 className="food-foodID-detail">{course.foodID}</h1></div>
              <h5 className="card-title food-nameFood-detail">
                {/* Sử dụng onClick để gọi hàm handleFoodDetailClick */}
                <Link
                  to={`/DetailFood?foodID=${course.foodID}&image=${course.imageFoodDB}&foodName=${course.foodName}&price=${formattedPrice}&description=${course.description}`}
                  onClick={() => handleFoodDetailClick(course)}
                >
                  {course.foodName}
                </Link>
              </h5>
              <div className="rate">
                <input type="radio" id="star5" name="rate" defaultValue="5" />
                <label htmlFor="star5" title="text">5 stars</label>
                <input type="radio" id="star4" name="rate" defaultValue="4" />
                <label htmlFor="star4" title="text">4 stars</label>
                <input type="radio" id="star3" name="rate" defaultValue="3" />
                <label htmlFor="star3" title="text">3 stars</label>
                <input type="radio" id="star2" name="rate" defaultValue="2" />
                <label htmlFor="star2" title="text">2 stars</label>
                <input type="radio" id="star1" name="rate" defaultValue="1" />
                <label htmlFor="star1" title="text">1 star</label>
              </div>
              <br />
              <div className="clearfix mb-3">
                <span className="badge rounded-pill food-price-detail">{formattedPrice}₫</span>
              </div>
              <p className="card-text">{shortenedDescription}</p>
              <a href="#" className="btn btn-danger mr-2" onClick={() => handleAddToCart(course.foodName)}> <i className="fa fa-shopping-cart"></i>Thêm giỏ hàng
              </a>
            </div>
          </div>
        </div>
      );
    })
  }

  /*** Function Delete product in Cart */
  const deleteCartItem = (orderItemID) => {
    axios.delete("http://localhost:8080/orderItems/deleteOrderItems", {
      params: { orderItemsID: orderItemID },
    }).then(() => {
      // Remove product in apiDetailFood
      const updatedDetailFood = apiDetailFood.filter((course) => course.orderItemID !== orderItemID);
      setDetailFood(updatedDetailFood);
    });
  }

  return (
    <BrowserRouter>
      <ModalCartFood
        apiDetailFood={apiDetailFood}
        showModal={showModal}
        closeModal={closeModal}
        deleteCartItem={deleteCartItem}
        totalPriceFormat={totalPriceFormat()}
      />
      {/* Header component parent */}
      <Routes>
        <Route path='/'
          element={<HeaderFood
            handleCartIconClick={handleCartIconClick}
            countFoodName={countFoodName}
          />}>
          <Route path="/"
            element={<Introduce
            />} />
            
          <Route path="/MenuFood"
            element={<MenuFood
              apiFood={apiFood}
              renderAPIFood={renderAPIFood}
              onFoodDetailClick={handleFoodDetailClick}
              handleAddToCart={handleAddToCart}
            />} />

          <Route path="/DetailFood"
            element={<DetailFood
              apiFood={apiFood}
              quantityValue={selectedQuantity}
              handleAddToCart={handleAddToCart}
              renderAPIFood={renderAPIFood}
              handleIncreaseValue={increaseValue}
              handleDecreaseValue={decreaseValue}
            />} />

          <Route path="/CartFood"
            element={<CartFood
              apiDetailFood={apiDetailFood}
              countFoodName={countFoodName}
              closeModal={closeModal}
              deleteCartItem={deleteCartItem}
              totalPriceFormat={totalPriceFormat()}
              totalPriceFinal={totalPriceFinal()}
              handleShippingChange={handleShippingChange}
              handleIncreaseValue={increaseValue}
              handleDecreaseValue={decreaseValue}
              setDetailFood={setDetailFood}
            />} />

          <Route path='/FoodManager'
            element={<FoodManager
              apiFood={apiFood}
              closeModal={closeModal}
              setApiFood={setApiFood}
              getAPIFood={getAPIFood}
            />} />
        </Route>

      </Routes>
      <Footer />
    </BrowserRouter>

  )
}
