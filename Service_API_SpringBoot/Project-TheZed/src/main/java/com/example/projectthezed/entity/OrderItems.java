package com.example.projectthezed.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderItems {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderItemID;
    private int quantity;
    private int total;
    private LocalDate orderDate;
    @Column(columnDefinition = "nvarchar(255)")
    private String status;

    @ManyToOne
    @JoinColumn(name = "foodID")
    private Food food;

    @ManyToOne
    @JoinColumn(name = "customerID ")
    private Customer customer;

    @OneToMany(mappedBy = "orderItems")
    private List<Invoices> invoices;
}
