package com.example.projectthezed.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BookingTable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int bookingID;
    private LocalDate bookingDate;
    private LocalTime bookingTime;
    private int quantityCustomer;
    private int quantityTable;
    @ManyToOne
    @JoinColumn(name = "customerID")
    private Customer customer;


}
