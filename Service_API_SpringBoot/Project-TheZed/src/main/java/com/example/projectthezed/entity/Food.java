package com.example.projectthezed.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "foodID")
    private int foodID;
    @Column(columnDefinition = "nvarchar(255)", name = "foodName")
    private String foodName;
    @Column(name = "price")
    private int price;
    @Column(columnDefinition = "nvarchar(255)", name = "imageFood")
    private String imageFood;
    @Column(columnDefinition = "nvarchar(2000)", name = "description")
    private String description;
    //Xac dinh dieu huong mapping voi nhau de JSON doc
//    @JsonManagedReference
    
//    @JsonIgnoreProperties("food") sẽ thông báo cho Jackson bỏ qua quá trình serialize của 
//    trường orderItems khi gặp đối tượng Food. Điều này giúp tránh vòng lặp vô tận trong quá trình serialize.
    @JsonIgnoreProperties("food")
    @OneToMany(mappedBy = "food")
    private List<OrderItems> orderItems;
    
    @JsonBackReference
    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "menuID")
    private Menu menu;
    

    //Su dung doc len JSON neu ton tai khong ton tai return 0
    public int getMenuId() {
        if (menu != null) {
            return menu.getMenuID();
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Food{" +
                "foodID=" + foodID +
                ", foodName='" + foodName + '\'' +
                ", price=" + price +
                ", imageFood='" + imageFood + '\'' +
                ", description='" + description + '\'' +
                ", menu=" + menu +
                '}';
    }
}
