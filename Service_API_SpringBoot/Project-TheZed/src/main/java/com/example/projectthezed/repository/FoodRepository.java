package com.example.projectthezed.repository;

import com.example.projectthezed.entity.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Interface FoodRepository.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Repository
public interface FoodRepository extends JpaRepository<Food, Integer> {

    /**
     * Find by food name.
     *
     * @param foodName the food name
     * @return the optional
     */
    Optional<Food> findByFoodName(String foodName);

    /**
     * Find all by menu name.
     *
     * @return the list
     */
    @Query(value = "SELECT f.foodID, f.price, f.description, f.foodName, f.imageFood, m.menuName " +
            "FROM Food f " +
            "JOIN Menu m ON f.menu.menuID = m.menuID")
    List<Object[]> findAllByMenuName();

}
