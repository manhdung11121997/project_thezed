package com.example.projectthezed.service;

import com.example.projectthezed.entity.Employee;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface EmployeeService.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public interface EmployeeService {

    /**
     * Gets the all employee.
     *
     * @return the all employee
     */
    List<Employee> getAllEmployee();

    /**
     * Adds the employee.
     *
     * @param employee the employee
     * @return the employee
     */
    Employee addEmployee(Employee employee);
}
