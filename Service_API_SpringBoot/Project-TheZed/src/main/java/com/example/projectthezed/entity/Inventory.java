package com.example.projectthezed.entity;

import java.time.LocalDate;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int itemID;
    @Column(columnDefinition = "nvarchar(255)")
    private String itemType;
    @Column(columnDefinition = "nvarchar(255)")
    private String itemName;
    private int quantity;
    private int price;
    @Column(columnDefinition = "nvarchar(255)")
    private String supplier;
    @Column(columnDefinition = "nvarchar(255)")
    private String status;
    private LocalDate expirationDate;
    private LocalDate importDate;
    private LocalDate lastUpdate;
    @OneToMany(mappedBy = "inventory", cascade = CascadeType.ALL)
    private List<Systems> systems;
}
