package com.example.projectthezed.controller;

import com.example.projectthezed.model.BookingTableDTO;
import com.example.projectthezed.service.BookingTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// TODO: Auto-generated Javadoc
/**
 * The Class BookingTableController.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@RestController
@CrossOrigin
@RequestMapping("/bookingTable")
public class BookingTableController {
    
    /** The booking table service. */
    @Autowired
    BookingTableService bookingTableService;

    /**
     * Adds the booking table.
     *
     * @param bookingTableDTO the booking table DTO
     * @return the response entity
     */
    @PostMapping("/addBookingTable")
    public ResponseEntity<String> addBookingTable(@RequestBody BookingTableDTO bookingTableDTO){
        System.err.println("BookingDto" + bookingTableDTO);
        bookingTableService.addBookingTable(bookingTableDTO);
        return ResponseEntity.ok("Add successfully");

    }
}
