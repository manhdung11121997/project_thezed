package com.example.projectthezed.service;

import java.util.List;

import com.example.projectthezed.model.OrderItemsDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface OrderItemService.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public interface OrderItemService {

    /**
     * Adds the order items.
     *
     * @param orderItemsDTO the order items DTO
     */
    void addOrderItems(OrderItemsDTO orderItemsDTO);

    /**
     * Gets the all order items.
     *
     * @return the all order items
     */
    List<OrderItemsDTO> getAllOrderItems();

    /**
     * Delete order items.
     *
     * @param orderItemsDTO the order items DTO
     */
    void deleteOrderItems(Integer orderItemsDTO);

    /**
     * Update order items.
     *
     * @param itemsDTO the items DTO
     * @param orderItemsID the order items ID
     */
    void updateOrderItems(OrderItemsDTO itemsDTO, Integer orderItemsID);

}
