package com.example.projectthezed.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Systems {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int systemID;
    @ManyToOne
    @JoinColumn(name = "customerID")
    private Customer customer;
    @ManyToOne
    @JoinColumn(name = "employeeID")
    private Employee employee;
    @ManyToOne
    @JoinColumn(name = "itemID")
    private Inventory inventory;
    @ManyToOne
    @JoinColumn(name = "menuID")
    private Menu menu;
}
