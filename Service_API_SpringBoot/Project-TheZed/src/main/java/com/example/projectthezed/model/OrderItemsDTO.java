package com.example.projectthezed.model;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class OrderItemsDTO {

    private int orderItemID;
    private int quantity;
    private int total;
    private LocalDate orderDate;
    private String status;
    private int foodID;
    private int customerID;
    private String  ImageFoodDB;
    private String foodName;
    private int price;

}
