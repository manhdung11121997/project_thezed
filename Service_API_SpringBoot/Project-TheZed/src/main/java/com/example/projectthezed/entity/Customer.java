package com.example.projectthezed.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int customerID;
    @Column(columnDefinition = "nvarchar(255)")
    private String fullName;
    @Column(columnDefinition = "nvarchar(255)")
    private String accountName;
    @Column(columnDefinition = "nvarchar(255)")
    private String email;
    @Column(columnDefinition = "nvarchar(255)")
    private String password;
    @Column(columnDefinition = "nvarchar(255)")
    private String phone;
    @Column(columnDefinition = "nvarchar(255)")
    private String address;
    @OneToMany(mappedBy = "customer")
    private List<Systems> systems;
    @OneToMany(mappedBy = "customer")
    private List<Reservations> reservations;
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<BookingTable> bookingTables;
    @OneToMany(mappedBy = "customer")
    private List<OrderItems> OrderItems;
    @OneToMany(mappedBy = "customer",  cascade = CascadeType.ALL)
    private List<Invoices> invoices;
}
