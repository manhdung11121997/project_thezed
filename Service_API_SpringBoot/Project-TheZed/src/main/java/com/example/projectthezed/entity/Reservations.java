package com.example.projectthezed.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Reservations {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int reservationID;
    private LocalDate reservationDate;
    private LocalTime reservationTime;
    private int numOfGuests;
    @Column(columnDefinition = "nvarchar(255)")
    private String status;
    @ManyToOne
    @JoinColumn(name = "customerID")
    private Customer customer;
    @OneToMany(mappedBy = "reservations")
    private List<Invoices> invoices;

}
