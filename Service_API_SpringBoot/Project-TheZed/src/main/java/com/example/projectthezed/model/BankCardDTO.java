package com.example.projectthezed.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class BankCardDTO {
    private int bankID;
    private MultipartFile imageQRCode;
    private String imageQRCodeDB;
    private String nameBank;
    private String accountNumber;
    private String accountName;
    private String invoicesID;
    private int totalAmount;

}