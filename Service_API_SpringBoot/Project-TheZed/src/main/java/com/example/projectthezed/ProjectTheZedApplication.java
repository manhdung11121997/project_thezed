package com.example.projectthezed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectTheZedApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectTheZedApplication.class, args);
    }

}
