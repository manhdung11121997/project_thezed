package com.example.projectthezed.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.projectthezed.entity.BankCard;
import com.example.projectthezed.model.BankCardDTO;
import com.example.projectthezed.service.BankCardService;

// TODO: Auto-generated Javadoc
/**
 * The Class BankCardController.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 1 thg 6, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@RestController
@CrossOrigin
@RequestMapping("/bankCard")
public class BankCardController {

    /** The bank card service. */
    @Autowired
    BankCardService bankCardService;

    /**
     * Addbank card.
     *
     * @param bankCardDTO the bank card DTO
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping("/addBankCard")
    public ResponseEntity<String> addbankCard(@ModelAttribute BankCardDTO bankCardDTO) throws Exception{
        System.err.println("DTO BANK CARD" +  bankCardDTO.toString());
        bankCardService.addBankCard(bankCardDTO);

        return ResponseEntity.ok("Add successfully");
    }

    /**
     * Show bank card.
     *
     * @return the response entity
     */
    @GetMapping("listBankCard")
    public ResponseEntity<List<BankCard>> showBankCard(){
        List<BankCard> dtoList = bankCardService.getAllBankCard();

        if (dtoList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(dtoList, HttpStatus.OK);
        }

    }
    
    /**
     * Show image bank.
     *
     * @param nameImageBank the name image bank
     * @return the response entity
     */
    @GetMapping("/{nameImageBank}")
    public ResponseEntity<?> showImageBank (@PathVariable String nameImageBank) {
        byte[] imageData = bankCardService.showImageBank(nameImageBank);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(imageData);
    }
}
