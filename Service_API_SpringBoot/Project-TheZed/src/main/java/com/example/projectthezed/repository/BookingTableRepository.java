package com.example.projectthezed.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.projectthezed.entity.BookingTable;

/**
 * The Interface BookingTableRepository.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Repository
public interface BookingTableRepository extends JpaRepository<BookingTable, Integer> {
}
