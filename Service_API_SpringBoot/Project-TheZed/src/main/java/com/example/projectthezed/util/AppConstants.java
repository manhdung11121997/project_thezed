package com.example.projectthezed.util;

/**
* @author        : DungLM6
* @from         : DN23_FR_JAVA_04
* @Created_date: 17 thg 7, 2023  09:20:13
* @System_Name    : Project-TheZed
* @Version        : 1.0
* @Create_by    : LENOVO
*/
public class AppConstants {
    public static final String DEFAULT_PAGE_NUMBER = "0";
    public  static final String DEFAULT_PAGE_SIZE = "10";

}
