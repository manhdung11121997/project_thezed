package com.example.projectthezed.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.projectthezed.entity.Food;


// TODO: Auto-generated Javadoc
/**
 * The Interface FoodRespositoryPaging.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public interface FoodRespositoryPaging extends PagingAndSortingRepository<Food, Integer> {
	
	/**
	 * Find all food with menu name.
	 *
	 * @param pageable the pageable
	 * @return the page
	 */
	@Query(value = "SELECT f.foodID, f.price, f.description, f.foodName, f.imageFood, m.menuName " +
	        "FROM Food f " +
	        "JOIN Menu m ON f.menu.menuID = m.menuID",
	        countQuery = "SELECT COUNT(f.foodID) FROM Food f JOIN Menu m ON f.menu.menuID = m.menuID")
	Page<Object[]> findAllFoodWithMenuName(Pageable pageable);
	
	
	/**
	 * Search food with menu name.
	 *
	 * @param keyword the keyword
	 * @param pageable the pageable
	 * @return the page
	 */
	@Query(value = "SELECT f.foodID, f.price, f.description, f.foodName, f.imageFood, m.menuName " +
            "FROM Food f " +
            "JOIN Menu m ON f.menu.menuID = m.menuID " +
            "WHERE f.foodID LIKE %:keyword% OR f.foodName LIKE %:keyword% OR m.menuName LIKE %:keyword%",
            countQuery = "SELECT COUNT(f.foodID) FROM Food f JOIN Menu m ON f.menu.menuID = m.menuID " +
            "WHERE f.foodID LIKE %:keyword% OR f.foodName LIKE %:keyword% OR m.menuName LIKE %:keyword%")
	Page<Object[]> searchFoodWithMenuName(@Param("keyword") String keyword, Pageable pageable);
}
