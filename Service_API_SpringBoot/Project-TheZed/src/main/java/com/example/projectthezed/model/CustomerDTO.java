package com.example.projectthezed.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CustomerDTO {
    private int customerID;
    private String fullName;
    private String accountName;
    private String email;
    private String password;
    private String phone;
    private String address;
    private String invoicesID;
    private int totalAmount;

}
