package com.example.projectthezed.model;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class FoodDTO {

    private int foodID;
    private String foodName;
    private String description;
    private int price;
    private int menuID;
    private MultipartFile imageFood;
    private String menuName;
    private String imageFoodDB;


}
