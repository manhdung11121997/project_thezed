package com.example.projectthezed.service;

import com.example.projectthezed.model.BookingTableDTO;


// TODO: Auto-generated Javadoc
/**
 * The Interface BookingTableService.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public interface BookingTableService {
   
   /**
    * Adds the booking table.
    *
    * @param bookingTableDTO the booking table DTO
    */
   void  addBookingTable(BookingTableDTO bookingTableDTO);



}
