package com.example.projectthezed.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BankCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int bankID;
    private String imageQRCode;
    @Column(columnDefinition = "nvarchar(255)")
    private String nameBank;
    private String accountNumber;
    @Column(columnDefinition = "nvarchar(255)")
    private String accountName;
    @OneToMany(mappedBy = "bankCard")
    private List<Invoices> invoices;
}
