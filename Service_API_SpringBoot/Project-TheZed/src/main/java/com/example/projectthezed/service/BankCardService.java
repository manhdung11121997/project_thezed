package com.example.projectthezed.service;

import com.example.projectthezed.entity.BankCard;
import com.example.projectthezed.model.BankCardDTO;

import java.io.IOException;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface BankCardService.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public interface BankCardService  {

    /**
     * Adds the bank card.
     *
     * @param bankCardDTO the bank card DTO
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void addBankCard(BankCardDTO bankCardDTO) throws IOException;


    /**
     * Gets the all bank card.
     *
     * @return the all bank card
     */
    List<BankCard> getAllBankCard();

    /**
     * Show image bank.
     *
     * @param nameImageBank the name image bank
     * @return the byte[]
     */
    byte[] showImageBank(String nameImageBank);

    }

