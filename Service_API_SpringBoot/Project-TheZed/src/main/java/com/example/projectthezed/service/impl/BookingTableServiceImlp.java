package com.example.projectthezed.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.projectthezed.entity.BookingTable;
import com.example.projectthezed.entity.Customer;
import com.example.projectthezed.model.BookingTableDTO;
import com.example.projectthezed.repository.BookingTableRepository;
import com.example.projectthezed.service.BookingTableService;

// TODO: Auto-generated Javadoc
/**
 * The Class BookingTableServiceImlp.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Service
public class BookingTableServiceImlp implements BookingTableService {

    /** The booking table repository. */
    @Autowired
    BookingTableRepository bookingTableRepository;


    /**
     * Adds the booking table.
     *
     * @param bookingTableDTO the booking table DTO
     */
    @Override
    public void addBookingTable(BookingTableDTO bookingTableDTO) {
        BookingTable bookingTable = new BookingTable();
        bookingTable.setQuantityCustomer(bookingTableDTO.getQuantityCustomer());
        bookingTable.setQuantityTable(bookingTable.getQuantityTable());
        bookingTable.setBookingDate(bookingTableDTO.getBookingDate());
        bookingTable.setBookingTime(bookingTableDTO.getBookingTime());
        Customer customer = new Customer();
        customer.setFullName(bookingTableDTO.getFullName());
        customer.setPhone(bookingTableDTO.getPhone());
        customer.setEmail(bookingTableDTO.getEmail());
        bookingTable.setCustomer(customer);

        bookingTableRepository.save(bookingTable);

    }
}
