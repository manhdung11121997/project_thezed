package com.example.projectthezed.controller;

import com.example.projectthezed.model.CustomerDTO;
import com.example.projectthezed.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerController.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@RestController
@CrossOrigin
@RequestMapping("/customer")
public class CustomerController {

    /** The customer service. */
    @Autowired
    CustomerService customerService;

    /**
     * Adds the customer.
     *
     * @param customerDTO the customer DTO
     * @return the response entity
     */
    @PostMapping("/addCustomer")
    public ResponseEntity<String> addCustomer(@RequestBody  CustomerDTO customerDTO){
        System.err.println(customerDTO.toString());
        customerService.addCustomer(customerDTO);
        return ResponseEntity.ok("Add successfully");
    }



}
