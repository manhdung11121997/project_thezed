package com.example.projectthezed.service;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;

import com.example.projectthezed.model.FoodDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface FoodService.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
public interface FoodService {

    /**
     * Gets the all foods.
     *
     * @return the all foods
     */
    List<FoodDTO> getAllFoods();

    /**
     * Update food.
     *
     * @param dto the dto
     * @param foodID the food ID
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void updateFood(FoodDTO dto, Integer foodID) throws IOException;

    /**
     * Delete food.
     *
     * @param foodID the food ID
     */
    void deleteFood(Integer foodID);
    
   /**
    * Gets the all food paging.
    *
    * @param pageNo the page no
    * @param pageSize the page size
    * @return the all food paging
    */
   Page<FoodDTO> getAllFoodPaging(int pageNo, int pageSize);
   
   /**
    * Search food with pagination.
    *
    * @param keyword the keyword
    * @param pageNo the page no
    * @param pageSize the page size
    * @return the page
    */
   Page<FoodDTO> searchFoodWithPagination(String keyword, int pageNo, int pageSize);
   
}


