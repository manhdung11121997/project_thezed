package com.example.projectthezed.model;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class BookingTableDTO {
    private int bookingID;
    private LocalDate bookingDate;
    private LocalTime bookingTime;
    private int quantityCustomer;
    private int quantityTable;
    private String fullName;
    private String phone;
    private String email;

}
