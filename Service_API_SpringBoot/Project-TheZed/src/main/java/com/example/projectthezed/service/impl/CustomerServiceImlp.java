package com.example.projectthezed.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.projectthezed.entity.Customer;
import com.example.projectthezed.entity.Invoices;
import com.example.projectthezed.model.CustomerDTO;
import com.example.projectthezed.repository.CustomerRespository;
import com.example.projectthezed.repository.InvoicesResponsitory;
import com.example.projectthezed.service.CustomerService;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerServiceImlp.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Service
public class CustomerServiceImlp implements CustomerService {
    
    /** The customer respository. */
    @Autowired
    CustomerRespository customerRespository;
    
    /** The invoices responsitory. */
    @Autowired
    InvoicesResponsitory invoicesResponsitory;


    /**
     * Adds the customer.
     *
     * @param customerDTO the customer DTO
     */
    @Override
    public void addCustomer(CustomerDTO customerDTO) {
        Customer customer = new Customer();

        customer.setFullName(customerDTO.getFullName());
        customer.setAccountName(customerDTO.getAccountName());
        customer.setEmail(customerDTO.getEmail());
        customer.setPhone(customerDTO.getPhone());
        customer.setAddress(customerDTO.getAddress());
        // set TotalAmount to Invoices
        Invoices invoices = new Invoices();
        invoices.setTotalAmount(customerDTO.getTotalAmount());
        //Set
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
        String formattedDate = dateFormat.format(calendar.getTime());
        System.out.println(formattedDate);
        invoices.setInvoicesID("HD".concat(formattedDate));
        System.err.println("HD".concat(formattedDate));
        invoices.setCustomer(customer);
//        invoicesResponsitory.save(invoices);
        List<Invoices> invoicesList = new ArrayList<>();
        invoicesList.add(invoices);
        customer.setInvoices(invoicesList);

        System.err.println("LISTCUSTOME"+invoicesList.toString());

        customerRespository.save(customer);

    }
}