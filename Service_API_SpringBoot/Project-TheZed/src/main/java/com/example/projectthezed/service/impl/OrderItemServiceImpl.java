package com.example.projectthezed.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.projectthezed.entity.Food;
import com.example.projectthezed.entity.OrderItems;
import com.example.projectthezed.model.OrderItemsDTO;
import com.example.projectthezed.repository.FoodRepository;
import com.example.projectthezed.repository.OrderItemsRepository;
import com.example.projectthezed.service.OrderItemService;

// TODO: Auto-generated Javadoc
/**
 * The Class OrderItemServiceImpl.
 * @author        : DungLM6 
 * @from         : DN23_FR_Batch_02
 * @Created_date: 25 thg 7, 2023  13:45:32 
 * @System_Name    : Final_Project_TheZed 
 * @Version        : 1.0 
 * @Create_by    : LENOVO
 */
@Service
public class OrderItemServiceImpl implements OrderItemService
{
    
    /** The order items repository. */
    @Autowired
    OrderItemsRepository orderItemsRepository;
    
    /** The food repository. */
    @Autowired
    FoodRepository foodRepository;


    /**
     * Adds the order items.
     *
     * @param dto the dto
     */
    @Override
    public void addOrderItems(OrderItemsDTO dto) {
        OrderItems orderItems = new OrderItems();
        orderItems.setOrderDate(LocalDate.now());
        // Set menu properties
        Food food = foodRepository.getReferenceById(dto.getFoodID());
//        System.err.println("MENU" + menu);
        orderItems.setFood(food);
        orderItems.setQuantity(dto.getQuantity());
        orderItems.setTotal(dto.getTotal());
        orderItemsRepository.save(orderItems);
    }

    /**
     * Gets the all order items.
     *
     * @return the all order items
     */
    @Override
    public List<OrderItemsDTO> getAllOrderItems() {
        List<Object[]> results = orderItemsRepository.findAllByFood();
        List<OrderItemsDTO> orderItem = new ArrayList<>();
        //Set to DB food
        for (Object[] result : results) {
            OrderItemsDTO itemsDTO = new OrderItemsDTO();
            itemsDTO.setImageFoodDB((String) result[0]);
            itemsDTO.setFoodName((String) result[1]);
            itemsDTO.setPrice((int) result[2]);
            itemsDTO.setQuantity((int) result[3]);
            itemsDTO.setTotal((int) result[4]);
            itemsDTO.setOrderItemID((int) result[5]);
            orderItem.add(itemsDTO);
        }
        return orderItem;
    }

    /**
     * Delete order items.
     *
     * @param orderItemsDTO the order items DTO
     */
    @Override
    public void deleteOrderItems(Integer orderItemsDTO) {
        orderItemsRepository.deleteById(orderItemsDTO);
    }

    /**
     * Update order items.
     *
     * @param itemsDTO the items DTO
     * @param orderItemID the order item ID
     */
    @Override
    public void updateOrderItems(OrderItemsDTO itemsDTO, Integer orderItemID) {
        Optional<OrderItems> orderItemsOptional =  orderItemsRepository.findById(orderItemID);
        if(orderItemsOptional.isPresent()){
            OrderItems orderItems =  orderItemsOptional.get();
            orderItems.setQuantity(itemsDTO.getQuantity());
            orderItems.setTotal(itemsDTO.getTotal());
            orderItemsRepository.save(orderItems);
        }




    }
}
